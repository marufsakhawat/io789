(function ($) {
    "use strict";
    $(document).ready(function () {
        


        // carousel-active
        $(".testimonial-carousel").owlCarousel({
            items: 1,
            loop: true,
            margin: 30,
            nav: false,
            navText: ['', ''],
            dots: true,
            autoplay: false,
        });
        
        // carousel-active
        $(".speech-carousel").owlCarousel({
            items: 1,
            loop: true,
            nav: true,
            navText: ['', ''],
            dots: true,
            autoplay: false,
        });
        
        $.scrollIt({
            upKey: 0, // key code to navigate to the next section
            downKey: 0, // key code to navigate to the previous section
            easing: 'linear', // the easing function for animation
            scrollTime: 900, // how long (in ms) the animation takes
            activeClass: 'active', // class given to the active nav element
            onPageChange: null, // function(pageIndex) that is called when page is changed
            onePage: true,
            topOffset: -95 // offste (in px) for fixed top navigation
        });
        
        // header-sticky
        $(window).on('scroll', function () {
            var scroll = $(window).scrollTop();
            if (scroll < 125) {
                $(".header-area").removeClass("header-sticky");
            } else {
                $(".header-area").addClass("header-sticky");
            }
        });
        
        
        // wow js active
		new WOW().init();
        
        // Page Preloader js
        function loader() {
            $(window).on('load', function () {
                $('#ctn-preloader').addClass('loaded');
                $("#loading").fadeOut(500);
                // Una vez haya terminado el preloader aparezca el scroll
                if ($('#ctn-preloader').hasClass('loaded')) {
                    // Es para que una vez que se haya ido el preloader se elimine toda la seccion preloader
                    $('#preloader').delay(900).queue(function () {
                        $(this).remove();
                    });
                }
            });
        }
        loader();
        
    }); //document ready function end

    $(window).load(function () {
        
    }); // winddow load function end
})(jQuery);